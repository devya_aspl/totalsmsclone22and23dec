package com.totalsmsclonedemo;

import android.app.Application;
import android.content.Context;

import com.totalsmsclonedemo.network.ApiClient;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TotalSMSCloneApp extends Application{

    private static TotalSMSCloneApp mInstance;
    private static Context mContext = null;
    private ApiClient mApiClient;

    public static synchronized TotalSMSCloneApp getInstance() {
        return mInstance;
    }
    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
        mInstance = this;
        initApiClient();
//
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SourceSansPro_Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void initApiClient() {
        mApiClient = new ApiClient(this);
    }

    public ApiClient getApiClient() {
        return mApiClient;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public TotalSMSCloneApp getApp() {
        return this;
    }

    public static Context getAppContext() {
        return mContext;
    }
}
