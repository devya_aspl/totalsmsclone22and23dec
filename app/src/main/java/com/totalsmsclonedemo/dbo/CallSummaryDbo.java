package com.totalsmsclonedemo.dbo;

public class CallSummaryDbo {

    String callImage;
    String callNumber,status, callTime, callDate, callStrTimeStamp;
    long callTimeStamp;

    public CallSummaryDbo(String callImage, String callNumber, String status, String callTime, String callDate) {
        this.callImage = callImage;
        this.callNumber = callNumber;
        this.status = status;
        this.callTime = callTime;
        this.callDate = callDate;
    }

    public CallSummaryDbo(String callImage, String callNumber, String status, String callTime, String callDate, long callTimeStamp) {
        this.callImage = callImage;
        this.callNumber = callNumber;
        this.status = status;
        this.callTime = callTime;
        this.callDate = callDate;
        this.callTimeStamp = callTimeStamp;
    }
    public CallSummaryDbo(String callImage, String callNumber, String status, String callTime, String callDate, long callTimeStamp, String callStrTimeStamp) {
        this.callImage = callImage;
        this.callNumber = callNumber;
        this.status = status;
        this.callTime = callTime;
        this.callDate = callDate;
        this.callTimeStamp = callTimeStamp;
        this.callStrTimeStamp = callStrTimeStamp;
    }



    public String getCallStrTimeStamp() {
        return callStrTimeStamp;
    }

    public void setCallStrTimeStamp(String callStrTimeStamp) {
        this.callStrTimeStamp = callStrTimeStamp;
    }

    public long getCallTimeStamp() {
        return callTimeStamp;
    }

    public void setCallTimeStamp(long callTimeStamp) {
        this.callTimeStamp = callTimeStamp;
    }

    public CallSummaryDbo() {

    }

    public String getCallImage() {
        return callImage;
    }

    public void setCallImage(String callImage) {
        this.callImage = callImage;
    }

    public String getCallNumber() {
        return callNumber;
    }

    public void setCallNumber(String callNumber) {
        this.callNumber = callNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCallTime() {
        return callTime;
    }

    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }

    public String getCallDate() {
        return callDate;
    }

    public void setCallDate(String callDate) {
        this.callDate = callDate;
    }
}
