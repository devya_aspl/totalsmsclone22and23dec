package com.totalsmsclonedemo.dbo;

public class CallAndSmsListDataDbo {
    int image;
    String phoneNumber,country,tier;

    public CallAndSmsListDataDbo(int image, String phoneNumber, String country, String tier) {
        this.image = image;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.tier = tier;
    }

    public CallAndSmsListDataDbo() {

    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }
}
