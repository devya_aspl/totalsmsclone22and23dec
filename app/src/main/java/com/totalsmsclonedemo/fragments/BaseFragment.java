package com.totalsmsclonedemo.fragments;


import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.totalsmsclonedemo.PermissionResult;
import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.TotalSMSCloneApp;
import com.totalsmsclonedemo.dialog.CustomDialog;
import com.totalsmsclonedemo.interfaces.MainInterfaces;
import com.totalsmsclonedemo.network.ApiClient;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment {

//    @Subscribe
    public abstract void setToolbarForFragment();
    public abstract void setTabLayout();
    public ApiClient mApiClient;
    private CustomDialog customDialog;
    private Handler handler;

    MainInterfaces mainInterfaces;
    private int KEY_PERMISSION = 0;
    private String permissionsAsk[];
    private PermissionResult permissionResult;


    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApiClient=getApp().getApiClient();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    protected void initDialog() {
        customDialog = new CustomDialog(getContext());
        handler = new Handler();
    }

    protected void dismissProgress() {
        if (handler != null && customDialog != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    customDialog.hide();
                }
            });
        }
    }

    protected void showProgress() {
        if (handler != null && customDialog != null) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (!customDialog.isDialogShowing()) {
                        customDialog.show();
                    }
                }// hideKeyboard(edt);}});}}
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base, container, false);
    }

    protected void showToastMsg(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    protected void showMsg(String msg) {
        showToastMsg(msg);
    }


    public void showSnackBar(View layout, String msg) {
        Snackbar.make(layout, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mainInterfaces = (MainInterfaces) getActivity();
        setToolbarForFragment();
        setTabLayout();
    }

    public TotalSMSCloneApp getApp() {
        return (TotalSMSCloneApp) getActivity().getApplication();
    }

    public boolean isPermissionsGranted(Context context, String permissions[]) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        boolean granted = true;
        for (String permission : permissions) {
            if (!(ActivityCompat.checkSelfPermission(
                    context, permission) == PackageManager.PERMISSION_GRANTED))
                granted = false;
        }

        return granted;
    }

    public void askCompactPermissions(String permissions[], PermissionResult permissionResult) {
        KEY_PERMISSION = 200;
        permissionsAsk = permissions;
        this.permissionResult = permissionResult;
        internalRequestPermission(permissionsAsk);
    }

    public boolean isPermissionGranted(Context context, String permission) {
        return (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) ||
                (ActivityCompat.checkSelfPermission(context, permission) ==
                        PackageManager.PERMISSION_GRANTED);
    }

    private void internalRequestPermission(String[] permissionAsk) {
        String arrayPermissionNotGranted[];
        ArrayList<String> permissionsNotGranted = new ArrayList<>();

        for (String aPermissionAsk : permissionAsk) {
            if (!isPermissionGranted(getContext(), aPermissionAsk)) {
                permissionsNotGranted.add(aPermissionAsk);
            }
        }

        if (permissionsNotGranted.isEmpty()) {

            if (permissionResult != null)
                permissionResult.permissionGranted();

        } else {
            arrayPermissionNotGranted = new String[permissionsNotGranted.size()];
            arrayPermissionNotGranted = permissionsNotGranted.toArray(arrayPermissionNotGranted);
            ActivityCompat.requestPermissions(getActivity(),
                    arrayPermissionNotGranted, KEY_PERMISSION);
        }
    }


}
