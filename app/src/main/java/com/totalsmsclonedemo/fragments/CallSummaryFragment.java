package com.totalsmsclonedemo.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.events.ApiErrorEvent;
import com.totalsmsclonedemo.events.ApiErrorWithMessageEvent;

import org.greenrobot.eventbus.Subscribe;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallSummaryFragment extends BaseFragment {

    View v;
    Unbinder unbinder;
    Context context;

    @Override
    public void setToolbarForFragment() {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getLl_toolbar().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getToolbartext().setText("Call Test Summary");

        ((BaseActivity)getActivity()).getImgToolBarBack().setVisibility(View.VISIBLE);
        ((BaseActivity)getActivity()).getImgToolBarBack().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainInterfaces.OpenDashBoard();
            }
        });
    }

    @Override
    public void setTabLayout() {

    }

    public CallSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_call_summary, container, false);
        context = (BaseActivity)getActivity();
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Subscribe
    public void onEventMainThread(ApiErrorEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                Log.e(":::","Error"+event.getRequestTag());
//                dismissProgress();
//                break;
            default:
                break;
        }
    }


    @Subscribe
    public void onEventMainThread(ApiErrorWithMessageEvent event) {
        switch (event.getRequestTag()) {
//            case LOGTAG:
//                dismissProgress();
//                Log.e(":::",""+event.getResultMsgUser());
//                break;
            default:
                break;
        }
    }

}
