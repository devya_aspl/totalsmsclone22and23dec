package com.totalsmsclonedemo.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyPhoneListenerService extends IntentService {

    public static final String TAG = "ForCallAndPhoneListener";
    public static int currentStatusOfPhone = 0;

    public MyPhoneListenerService() {
        super("CallAndPhoneListener");
    }

    PhoneListener phoneListener = null;


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try{

//            Bundle b = intent.getExtras();
//            String phoneNumber = b.getString("phoneNumber");
//            long duration = b.getLong("duration");
//            Log.e(TAG, "gotData phoneNumber: " + phoneNumber);
//            Log.e(TAG, "gotData duration: " + duration);
//            phoneListener = new PhoneListener();
//            Log.e(TAG,"PhoneListener: "+currentStatusOfPhone);
//
//            Intent intent2 = new Intent(Intent.ACTION_DIAL);
//            intent2.setData(Uri.parse("tel:" + phoneNumber));
//            this.startActivity(intent2);


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

   public class PhoneListener extends PhoneStateListener {
//       public PhoneListener() {
//
//       }


        String TAG = getClass().getName();

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            Log.d(TAG, "PhoneListener::onCallStateChanged state:" + state + " incomingNumber:" + incomingNumber);
            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    currentStatusOfPhone = 0;
                    Log.e(TAG, "IDLE");
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.e(TAG, "OFFHOOK");
                    Log.e(TAG, "CALL_STATE_OFFHOOK starting recording");
                    currentStatusOfPhone = 2;
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.e(TAG, "RINGING");
                    currentStatusOfPhone = 1;
                    Log.e(TAG, "setPhoneState is called while call state is ringing");
                    break;
            }
        }



//        @Override
//    protected void onHandleIntent(@Nullable Intent intent) {
//
//    }
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        // do what you need here
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId)
    {

        try{
            Bundle b = intent.getExtras();
            String phoneNumber = b.getString("phoneNumber");
            long duration = b.getLong("duration");
            Log.e(TAG, "gotData phoneNumber: " + phoneNumber);
            Log.e(TAG, "gotData duration: " + duration);
            phoneListener = new PhoneListener();
            Log.e(TAG,"PhoneListener: "+currentStatusOfPhone);

            Intent intent2 = new Intent(Intent.ACTION_DIAL);
            intent2.setData(Uri.parse("tel:" + phoneNumber));
            this.startActivity(intent2);
        }
        catch (Exception e)
        {

        }

        if (phoneListener == null)
        {
            TelephonyManager tm = (TelephonyManager)getApplicationContext().getSystemService(TELEPHONY_SERVICE);
            phoneListener = new PhoneListener();
            tm.listen(phoneListener,PhoneStateListener.LISTEN_CALL_STATE);
        }
        // do what you need to do here

        return START_STICKY; // you can set it as you want
    }
}
