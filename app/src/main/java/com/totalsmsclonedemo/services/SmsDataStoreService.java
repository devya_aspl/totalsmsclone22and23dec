package com.totalsmsclonedemo.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.totalsmsclonedemo.Utilities;
import com.totalsmsclonedemo.databases.DBHelperForListForSms;
import com.totalsmsclonedemo.databases.DBHelperForSMSCountryNameDetails;
import com.totalsmsclonedemo.databases.DBHelperForSmsTierNameDetails;
import com.totalsmsclonedemo.dbo.CallListDataDbo;
import com.totalsmsclonedemo.dbo.SmsListDataDbo;
import com.totalsmsclonedemo.dialog.CustomDialog;
import com.totalsmsclonedemo.network.ApIServiceNew;
import com.totalsmsclonedemo.network.response.CallListResponseData;
import com.totalsmsclonedemo.network.response.CallListResponseNew;
import com.totalsmsclonedemo.network.response.SmsListResponse;
import com.totalsmsclonedemo.network.response.SmsListResponseData;
import com.totalsmsclonedemo.network.response.SmsListResponseNew;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SmsDataStoreService extends IntentService {

    public static final String TAG = "callDataIntent";
    DBHelperForListForSms dbHelperForListForSms;
    DBHelperForSmsTierNameDetails dbHelperForSmsTierNameDetails;
    DBHelperForSMSCountryNameDetails dbHelperForSMSCountryNameDetails;
    ApIServiceNew newsAPI;
    private CustomDialog customDialog;
    private Handler handler;


    public SmsDataStoreService() {
        super("SmsDataStoreService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try {
//            customDialog = new CustomDialog(this);
//            handler = new Handler();
//            showProgress();
            dbHelperForListForSms = new DBHelperForListForSms(this);
            dbHelperForSmsTierNameDetails = new DBHelperForSmsTierNameDetails(this);
            dbHelperForSMSCountryNameDetails = new DBHelperForSMSCountryNameDetails(this);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://tapsglobalsolutions.com/iprnsms/ws/webservice/testnumbers/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            newsAPI = retrofit.create(ApIServiceNew.class);

            if (Utilities.isNetworkAvailable(getApplicationContext())) {
                Log.e(TAG, "check internet............");
                new smsListAsync().execute();
            }

//            dismissProgress();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
//        try {
////            customDialog = new CustomDialog(this);
////            handler = new Handler();
////            showProgress();
//            dbHelperForListForSms = new DBHelperForListForSms(this);
//            dbHelperForSmsTierNameDetails = new DBHelperForSmsTierNameDetails(this);
//            dbHelperForSMSCountryNameDetails = new DBHelperForSMSCountryNameDetails(this);
//
//            Retrofit retrofit = new Retrofit.Builder()
//                    .baseUrl("http://tapsglobalsolutions.com/iprnsms/ws/webservice/testnumbers/")
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//            newsAPI = retrofit.create(ApIServiceNew.class);
//
//            if (Utilities.isNetworkAvailable(getApplicationContext())) {
//                Log.e(TAG, "check internet............");
//                new smsListAsync().execute();
//            }
//
////            dismissProgress();
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private class smsListAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                Call<SmsListResponseNew> call_list = newsAPI.get_sms_list();

                call_list.enqueue(new Callback<SmsListResponseNew>() {
                    @Override
                    public void onResponse(Call<SmsListResponseNew> call, Response<SmsListResponseNew> response) {
                        try {
                            if (response.isSuccessful()) {
                                Log.e(TAG, "response: " + response.body().getMsg());

                                List<SmsListResponseData> data = new ArrayList<>();
                                data = response.body().getData();

                                Log.e(TAG,"list data size: "+data.size());
                                Log.e(TAG,"list data size: "+data.size());
                                dbHelperForListForSms.deleteSMSList();
                                dbHelperForSmsTierNameDetails.deleteSmsTierNames();
                                dbHelperForSMSCountryNameDetails.deleteSmsCountryNames();
                                for(int i=0;i < data.size();i++)
                                {
                                    Log.e(TAG,"list data size: "+data.size());
//            int image, String phoneNumber, String country, String tier
                                    Log.e(TAG,"sms list database size: "+dbHelperForListForSms.getSmsCount());
                                    String countryImage = response.body().getPath() + data.get(i).getCountryImage();
                                    Log.e(TAG,"CountryImage: "+countryImage);
                                    dbHelperForListForSms.insertSmsListDetails(new SmsListDataDbo(countryImage,data.get(i).getNumber(),data.get(i).getCountry(),data.get(i).getTier(),false));
                                    dbHelperForSMSCountryNameDetails.insertSmsCountryName(data.get(i).getCountry());
                                    dbHelperForSmsTierNameDetails.insertSmsTierName(data.get(i).getTier());
                                    Log.e(TAG,"Size of Country names list: "+dbHelperForSMSCountryNameDetails.getSmsCountryNameCount());
                                    Log.e(TAG,"Size of tier names list: "+dbHelperForSmsTierNameDetails.getSmsTierNameCount());
                                    Log.e(TAG,"call list database size: "+dbHelperForListForSms.getSmsCount());
                                    Log.e(TAG,"Size of "+data.get(0).getTier() + " is "+ dbHelperForListForSms.getSmsTierCount(data.get(0).getTier()));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<SmsListResponseNew> call, Throwable t) {
                        Log.e(TAG, "response: " + "failure");
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.e(TAG,"sms service starting");
//        Toast.makeText(this, "sms service starting", Toast.LENGTH_SHORT).show();
        try {
//            customDialog = new CustomDialog(this);
//            handler = new Handler();
//            showProgress();
            dbHelperForListForSms = new DBHelperForListForSms(this);
            dbHelperForSmsTierNameDetails = new DBHelperForSmsTierNameDetails(this);
            dbHelperForSMSCountryNameDetails = new DBHelperForSMSCountryNameDetails(this);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://tapsglobalsolutions.com/iprnsms/ws/webservice/testnumbers/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            newsAPI = retrofit.create(ApIServiceNew.class);

            if (Utilities.isNetworkAvailable(getApplicationContext())) {
                Log.e(TAG, "check internet............");
                new smsListAsync().execute();
            }

//            dismissProgress();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }
}
