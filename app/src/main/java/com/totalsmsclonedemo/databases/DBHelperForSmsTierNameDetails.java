package com.totalsmsclonedemo.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DBHelperForSmsTierNameDetails extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 10678;

    Context context;

    // Database Name
    private static final String DATABASE_NAME = "sms_tierName_db";
    private static final String TABLE_NAME = "appSmsTierName";

//    ProjectConfigurationDbo configurationDbo;
    String tierName;

//    timerId,syncWithMobileData,autoSync,periodicity,chooseLanguage
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(tierName TEXT UNIQUE);";

    public DBHelperForSmsTierNameDetails(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("database","sms tier list table created");
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

//    tierName
    public void insertSmsTierName(String tierName) {


        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Log.e("insert sms tier:",
                    " tierName is:  "+ tierName
            );
            ContentValues values = new ContentValues();
            values.put("tierName", tierName);
            long rowInserted =   db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
//            if(rowInserted != -1)
//                Toast.makeText(context, "Data Inserted Successfully", Toast.LENGTH_SHORT).show();
//            else
//                Toast.makeText(context, "Something Wrong", Toast.LENGTH_SHORT).show();

            db.close();
//            Toast.makeText();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

//    public int getConfigurationCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }

//
    public ArrayList<String> getAllSmsTierName() {
        ArrayList<String> smsTierNameList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String tierName = cursor.getString(cursor.getColumnIndex("tierName"));
                smsTierNameList.add(tierName);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return call list
        return smsTierNameList;
    }

    public int getSmsTierNameCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int deleteSmsTierNames()
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

//            return db.delete(TABLE_NAME, "status" + " = ?", new String[]{"Pending"});

//            return db.delete(TABLE_NAME,
//                    KEY_DATE + "='date' AND " + KEY_GRADE + "='style2' AND " +
//                            KEY_STYLE + "='style' AND " + KEY_PUMPLEVEL + "='pumpLevel'",
//                    null);

            return db.delete(TABLE_NAME,
                    null,
                    null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return 0;
    }
}
