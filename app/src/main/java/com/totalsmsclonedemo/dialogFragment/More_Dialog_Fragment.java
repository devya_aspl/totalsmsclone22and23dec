package com.totalsmsclonedemo.dialogFragment;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.totalsmsclonedemo.R;
import com.totalsmsclonedemo.activities.BaseActivity;
import com.totalsmsclonedemo.interfaces.MainInterfaces;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by aspl on 17/1/18.
 */

public class More_Dialog_Fragment extends Dialog{

    public Activity c;
    public Dialog d;

    @BindView(R.id.tv_call)
    TextView tv_call;

    @BindView(R.id.tv_sms)
    TextView tv_sms;

    MainInterfaces mainInterfaces;

    public More_Dialog_Fragment(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_more);
        mainInterfaces = (MainInterfaces) c;
        ButterKnife.bind(this);
    }

    @OnClick(R.id.tv_call)
    public void onClickCall(View v)
    {
        mainInterfaces.OpenCallSummary();
        dismiss();
    }

    @OnClick(R.id.tv_sms)
    public void onClicksms(View v)
    {
        mainInterfaces.OpenSmsSummary();
        dismiss();
    }


}