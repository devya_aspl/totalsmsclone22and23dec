package com.totalsmsclonedemo.recyclerview_selection;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.totalsmsclonedemo.Model.ContactCallModel;

import java.util.List;

import androidx.recyclerview.selection.ItemKeyProvider;

/**
 * A basic implementation of {@link ItemKeyProvider} for String items.
 */

public class ContactItemKeyProvider extends ItemKeyProvider<ContactCallModel> {

    private List<ContactCallModel> items;

    public ContactItemKeyProvider(int scope, List<ContactCallModel> items) {
        super(scope);
        this.items = items;
    }

    @Nullable
    @Override
    public ContactCallModel getKey(int position) {
        return items.get(position);
    }

    @Override
    public int getPosition(@NonNull ContactCallModel key) {
        try{
        return items.indexOf(key);}
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return RecyclerView.NO_POSITION;

//        RecyclerView.ViewHolder viewHolder = recyclerList.findViewHolderForItemId(key);
//        return viewHolder == null ? RecyclerView.NO_POSITION : viewHolder.getLayoutPosition();
    }
}
