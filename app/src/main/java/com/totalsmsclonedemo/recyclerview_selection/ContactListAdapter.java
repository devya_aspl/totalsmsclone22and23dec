package com.totalsmsclonedemo.recyclerview_selection;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.totalsmsclonedemo.Model.ContactCallModel;
import com.totalsmsclonedemo.R;

import java.lang.reflect.Method;
import java.util.ArrayList;

import androidx.recyclerview.selection.SelectionTracker;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    Context context;
    ArrayList<ContactCallModel> data;
    CountDownTimer timer;

    private SelectionTracker mSelectionTracker;

    public ContactListAdapter(Context context, ArrayList<ContactCallModel> data) {
        this.context = context;
        this.data = data;
    }

    public void setSelectionTracker(SelectionTracker mSelectionTracker) {
        this.mSelectionTracker = mSelectionTracker;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {
            final ContactCallModel item = data.get(position);
            holder.bind(item, mSelectionTracker.isSelected(item));
            holder.text_view_country.setText(data.get(position).getContactCountry());
            holder.tv_number.setText(data.get(position).getContactNumber());

            holder.ll_call_list_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String phoneNumber = item.getContactNumber();
                    phoneNumber = phoneNumber.replace(" ", "");
                    phoneNumber = phoneNumber.replace("+", "");
                    phoneNumber =  phoneNumber.substring(2);
                    Log.e("callAdapter","Phone Number is: "+phoneNumber);
                    makeCall(phoneNumber,position);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void makeCall(String phoneNumber,int position) {

        try {
//        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse("tel:"+phoneNumber));
//        context.startActivity(intent);

            //performs call
            if (!phoneNumber.equals("")) {

//            Intent intent = new Intent(context, MyReceiver.class);
//            PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
//            AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
//            int milliseconds =  60 * 1000;
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
//                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);
//                    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//                alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);
//                    else
//                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);

//            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);

                Intent intent2 = new Intent(Intent.ACTION_DIAL);
                intent2.setData(Uri.parse("tel:" + phoneNumber));
                context.startActivity(intent2);


//                if (Utilities.getDisconnectDurationTime(((BaseActivity))context))

                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }

                timer = new CountDownTimer(60000, 1000) {
                    @Override
                    public void onTick(long l) {
                        Log.e("seconds remaining : ", "seconds remaining : " + l / 1000);
                    }

                    @Override
                    public void onFinish() {
                        Log.e("done!", "done!");
                        endCall(context);
                    }
                }.start();
//
//          timer =  new CountDownTimer(120000, 1000) {
//
//                public void onTick(long millisUntilFinished) {
//                    Log.e("adapter","seconds remaining: " + millisUntilFinished / 1000);
//                    //here you can have your logic to set text to edittext
//                }
//
//                public void onFinish() {
//                    Log.e("adapter","done!");
////                    endCall(context);
////                    killCall(context);
//                    try {
//                        declinePhone(context);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }.start();
//            timer.cancel();

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    public void endCall(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            Object telephonyService = m.invoke(tm);

            c = Class.forName(telephonyService.getClass().getName());
            m = c.getDeclaredMethod("endCall");
            m.setAccessible(true);
            m.invoke(telephonyService);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void filterList(ArrayList<ContactCallModel> filteredData) {
        data = filteredData;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout ll_call_list_item;
        TextView tv_number,text_view_country;

        ViewHolder(@NonNull View v) {
            super(v);
            ll_call_list_item = (LinearLayout)v.findViewById(R.id.ll_call_list_item);
            tv_number = (TextView)v.findViewById(R.id.tv_number);
            text_view_country = (TextView)v.findViewById(R.id.text_view_country);

        }

        void bind(ContactCallModel item, boolean isSelected) {
//            textView.setText(item);
            // If the item is selected then we change its state to activated
            ll_call_list_item.setActivated(isSelected);
        }

//        /**
//         * Create a new {@link StringItemDetails} for each string item, will be used later by {@link StringItemDetailsLookup#getItemDetails(MotionEvent)}
//         * @return {@link StringItemDetails} instance
//         */
        ContactItemDetails getItemDetails() {
            return new ContactItemDetails(getAdapterPosition(), data.get(getAdapterPosition()));
        }
    }
}
