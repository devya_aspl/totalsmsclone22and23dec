package com.totalsmsclonedemo.interfaces;

/**
 * Created by aspl on 10/1/18.
 */

public interface MainInterfaces {

    void OpenSplash();

//    void OpenLogin_Screen();

    void OpenDashBoard();

    void OpenCall();

    void OpenSms();

    void OpenConfiguration();

    void OpenMore();

    void OpenSmsSummary();
    void OpenCallSummary();

//    void OpenMenu();
//    void OpenDetailFragment();
//    void OpenTermsAndConditions();
//    void OpenContact();
//
//    void OpenDashboard_new_Activity();
//
//    void OpenChaptersActivity(String project_id, String project_title);

}
