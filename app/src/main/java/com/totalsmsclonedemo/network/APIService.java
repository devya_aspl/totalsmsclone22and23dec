package com.totalsmsclonedemo.network;

import com.totalsmsclonedemo.network.response.CallListResponse;
import com.totalsmsclonedemo.network.response.SmsListResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * The API interface for retrofit calls. This interface defines all the api calls made by the app.
 * Note: You have to pass a null value for a parameter to be skipped. Parameter values are URL
 * encoded by default. TODO: We may have to encode the parameter values before passing them to the
 * api calls here although they are URL encoded by retrofit automatically. We have to homemenu_fragment this
 * thoroughly.
 */
public interface APIService {

    @POST("CALL")
    Call<CallListResponse> get_call_list();

      @POST("SMS")
    Call<SmsListResponse> get_sms_list();




//    @POST("login")
//    Call<LoginResponse> login(@Body LoginModel data);

//    // get_project_details_by_deviceid
//  @POST("get_project_details_by_deviceid")
//    Call<ProjectListResponse> get_project_by_device_id(@Body ProjectsListModel data);
//
////    https://tapsglobalsolutions.com/presto/ws/webservice/get_project_details_by_device_new
//  @POST("get_project_details_by_device_new")
//    Call<NewProjectResponse> get_project_details_by_device_new(@Body ProjectsListModel data);
//
//        @Multipart
//        @POST("demo_file_upload")
//        Call<ProjectInsertDataResponse> demo_file_upload(
//                @Part MultipartBody.Part flfile);
//
////    http://192.168.1.63/presto/ws/webservice/get_project_details_by_deviceid
////    https://tapsglobalsolutions.com/presto/ws/webservice/project_file_data_insert
//       @Multipart
//        @POST("project_details_insert")
//        Call<ProjectInsertDataResponse> project_details_insert(
//               @Part("guid_id") RequestBody guid_id,
//               @Part("device_id") RequestBody device_id,
//               @Part("licence_key") RequestBody licence_key,
//               @Part("licence_code") RequestBody licence_code,
//               @Part("api_from") RequestBody api_from,
//               @Part("file_type") RequestBody file_type,
//               @Part MultipartBody.Part project_file,
//               @Part("description") RequestBody description,
//               @Part("details_guid_id") RequestBody details_guid_id
//       );
//
//
//    @Multipart
//    @POST("project_file_data_insert")
//    Call<ProjectInsertDataResponse> project_file_data_insert(
//            @Part("guid_id") RequestBody guid_id,
//            @Part("device_id") RequestBody device_id,
//            @Part("licence_key") RequestBody licence_key,
//            @Part("licence_code") RequestBody licence_code,
//            @Part("api_from") RequestBody api_from,
//            @Part("file_type") RequestBody file_type,
//            @Part MultipartBody.Part project_file,
//            @Part("description") RequestBody description,
//            @Part("details_guid_id") RequestBody details_guid_id,
//            @Part("chapter_guid_id") RequestBody chapter_guid_id,
//            @Part("subchapter_guid_id") RequestBody subchapter_guid_id,
//            @Part("workitem_guid_id") RequestBody workitem_guid_id,
//            @Part("code") RequestBody code
//
//    );
//
////    https://tapsglobalsolutions.com/presto/ws/webservice/contact_us
//
//    @FormUrlEncoded
//    @POST("contact_us")
//    Call<ContactResponse> contact_us(
//            @Field("reason") String reason,
//            @Field("comment") String comment,
//            @Field("licence_key") String licence_key,
//            @Field("licence_code") String licence_code,
//            @Field("api_from") String api_from,
//            @Field("device_id") String device_id);
//
//
//
////    @Multipart
////    @POST("memo_insert")
////    Call<MemoInsertResponse> memo_insert(
////            @Part("user_id") RequestBody user_id,
////            @Part("note") RequestBody note,
////            @Part("memo_name") RequestBody memo_name,
////            @Part("description") RequestBody description,
////            @Part("is_favourite") RequestBody is_favourite,
////            @Part MultipartBody.Part memo_file,
////            @Part("time") RequestBody time,
////            @Part("size") RequestBody size
//
//
//    /*====================================================================================================================
//   * Phase-1 APIS*/
////    @FormUrlEncoded
////    @POST("register")
////    Call<RegisterResponse> register(
////            @Field("name") String name,
////            @Field("password") String password,
////            @Field("email") String email,
////            @Field("mobile") String mobile,
////            @Field("device_id") String device_id,
////            @Field("login_with") String login_with,
////            @Field("device_type") String device_type,
////            @Field("user_image") String user_image,
////            @Field("social_id") String social_id
////    );
////
////        //insertMemo
////        @Multipart
////        @POST("memo_insert")
////        Call<MemoInsertResponse> memo_insert(
////        @Part("user_id") RequestBody user_id,
////        @Part("note") RequestBody note,
////        @Part("memo_name") RequestBody memo_name,
////        @Part("description") RequestBody description,
////        @Part("is_favourite") RequestBody is_favourite,
////        @Part MultipartBody.Part memo_file,
////        @Part("time") RequestBody time,
////        @Part("size") RequestBody size);

}

