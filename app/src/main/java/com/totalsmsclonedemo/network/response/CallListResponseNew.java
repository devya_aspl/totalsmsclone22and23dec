package com.totalsmsclonedemo.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CallListResponseNew extends AbstractApiResponse{

    @SerializedName("data")
    @Expose
    private List<CallListResponseData> data = null;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("msg")
    @Expose
    private String msg;

    public List<CallListResponseData> getData() {
        return data;
    }

    public void setData(List<CallListResponseData> data) {
        this.data = data;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
