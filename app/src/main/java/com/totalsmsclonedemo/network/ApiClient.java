package com.totalsmsclonedemo.network;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.totalsmsclonedemo.BuildConfig;
import com.totalsmsclonedemo.events.RequestFinishedEvent;
import com.totalsmsclonedemo.network.request.AbstractApiRequest;
import com.totalsmsclonedemo.network.request.CallListRequest;
import com.totalsmsclonedemo.network.request.SmsListRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Provides request functions for all api calls. This class maintains a map of running requests to
 * provide request cancellation.
 */
public class ApiClient {

    private static final String LOGTAG = ApiClient.class.getSimpleName();

    private static final int HTTP_TIMEOUT = 30;

    // Cache size for the OkHttpClient
    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB

    /*LIVE URL*/
    public static final String API_BASE_URL ="http://tapsglobalsolutions.com/iprnsms/ws/webservice/testnumbers/";

    /**
     * Makes the APIService calls.`
     */
    private final APIService mAPIService;

    /**
     * The list of running requests. Used to cancel requests.
     */
    private final Map<String, AbstractApiRequest> requests;

    private static OkHttpClient httpClient;
    private static Retrofit.Builder builder;

    private static Interceptor requestInterceptor = new Interceptor() {
        @Override
          public Response intercept(Chain chain) throws IOException {
            Response response = chain.proceed(chain.request());
            /*// Request Intercepting...
            JSONArray similararticle = null;
            ResponseBody body;
            Request original = chain.request();

            // Response Intercepting...
            response = chain.proceed(original);

            String res = response.body().string();

            JSONObject jMainObj = null;
            try {
                *//*jMainObj = new JSONObject(res);
                if (original.url().toString().contains("get_all_public_forum")) {

                    JSONArray jMainArray =  jMainObj.getJSONArray("info");
                    for (int i = 0; i < jMainArray.length(); i++) {
                        JSONObject object;
                        t  similararticle = object.getJSONArray("similar_article");

                            for (int j = 0; j < similararticle.length(); j++) {

                                JSONObject jsonObjectData = similararticle.getJSONObject(j);

                                if (jsonObjectData.has("similar_article")) {
                                    Log.e("ApiClient", "&&&&&& &&&&&& intercept: " + jsonObjectData.getString("similar_article"));
                                }
                                else {
                                    Log.e("ApiClient", "intercept, no similar article available...");
                                }
                            }ry {
                            object = jMainArray.getJSONObject(i);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }*//*

                MediaType contentType = response.body().contentType();
                body = ResponseBody.create(contentType, res);

                return response.newBuilder().body(body).build();
            } catch (Exception e) {
                e.printStackTrace();
            }
*/

            return response;


        }
        // Do anything with response here


    };

    public ApiClient(Application app) {

        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder().cache(cache);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okBuilder.addInterceptor(loggingInterceptor);
                    //.addInterceptor(requestInterceptor);
        }
        //okBuilder.addInterceptor(requestInterceptor);
        okBuilder.addInterceptor(requestInterceptor);
        httpClient = okBuilder
                .connectTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
                .build();

        builder = new Retrofit.Builder();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create();

        retrofit = builder
                .addConverterFactory(gsonConverterFactory)
                .baseUrl(API_BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        mAPIService = retrofit.create(APIService.class);
        requests = new HashMap<>();
        EventBus.getDefault().register(this);
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    static Retrofit retrofit;

/*
* ==========================================================================================================================
* API METHODS
* ==========================================================================================================================
* */

    /*Registration API Method*/


    public void get_call_list(String requestTag) {
        CallListRequest request = new CallListRequest(mAPIService, requestTag);
        requests.put(requestTag, request);
        request.execute();
    }

    public void get_sms_list(String requestTag) {
        SmsListRequest request = new SmsListRequest(mAPIService, requestTag);
        requests.put(requestTag, request);
        request.execute();
    }




//
//    public void get_project_by_device_id(String requestTag, ProjectsListModel data) {
//        ProjectsListRequest request = new ProjectsListRequest(mAPIService, requestTag,data);
//        requests.put(requestTag, request);
//        request.execute();
//    }
//
//    public void get_project_by_device_id_new(String requestTag, ProjectsListModel data) {
//        NewProjectsListRequest request = new NewProjectsListRequest(mAPIService, requestTag,data);
//        requests.put(requestTag, request);
//        request.execute();
//    }
//
////    guid_id, device_id, licence_key, licence_code, api_from, file_type, project_file
////String guid_id,String device_id,String licence_key,String licence_code,String api_from,String file_type,String project_file) {
//    public void project_details_insert(String requestTag, String guid_id, String device_id, String licence_key, String licence_code, String api_from, String file_type, String project_file, String description, String details_guid_id)
//    {
//        ProjectDataInsertRequest request = new ProjectDataInsertRequest(mAPIService, requestTag,guid_id,device_id,licence_key,licence_code,api_from,file_type,project_file,description,details_guid_id);
//        requests.put(requestTag, request);
//        request.execute();
//    }
//
//    public void project_file_data_insert(String requestTag, String guid_id, String device_id, String licence_key, String licence_code, String api_from, String file_type, String project_file, String description, String details_guid_id, String chapter_guid_id, String subchapter_guid_id, String workitem_guid_id, String code)
//    {
//        ProjectFileDataInsertRequest request = new ProjectFileDataInsertRequest(mAPIService, requestTag,guid_id,device_id,licence_key,licence_code,api_from,file_type,project_file,description,details_guid_id,chapter_guid_id,subchapter_guid_id,workitem_guid_id,code);
//        requests.put(requestTag, request);
//        request.execute();
//    }
//
//    public void contact_us(String requestTag, String reason, String comment, String licence_key, String licence_code, String api_from, String device_id)
//    {
//        ContactRequest request = new ContactRequest(mAPIService, requestTag,reason,comment,licence_key,licence_code,api_from,device_id);
//        requests.put(requestTag, request);
//        request.execute();
//    }
//
//    public void demo_file_upload(String requestTag, String flfile) {
//        DemoInsertImageRequest request = new DemoInsertImageRequest(mAPIService, requestTag,flfile);
//        requests.put(requestTag, request);
//        request.execute();
//    }
//
////    name,password,email,mobile,device_id,login_with,device_type,user_image,social_id
//    public void register(String requestTag,String name,String password,String email,String mobile,String device_id,String login_with,String device_type,String user_image,String social_id)
//    {
//        RegisterRequest request = new RegisterRequest(mAPIService,requestTag,name,password,email,mobile,device_id,login_with,device_type,user_image,social_id);
//        requests.put(requestTag,request);
//        request.execute();
//    }




    // ============================================================================================
    // Public functions
    // ============================================================================================


     /** Look up the event with the passed tag in the event list. If the request is found, cancel it
     * and remove it from the list.
     *
     * @param requestTag Identifies the request.
     * @return True if the request was cancelled, false otherwise.
     **/
    public boolean cancelRequest(String requestTag) {
        System.gc();
        AbstractApiRequest request = requests.get(requestTag);

        if (request != null) {
            request.cancel();
            requests.remove(requestTag);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns true if the request with the passed tag is in the list of running requests, false
     * otherwise.
     */
    public boolean isRequestRunning(String requestTag) {
        return requests.containsKey(requestTag);
    }


    /**
     * A request has finished. Remove it from the list of running requests.
     *
     * @param event The event posted on the EventBus.
     */
    @Subscribe
    public void onEvent(RequestFinishedEvent event) {
        System.gc();
        requests.remove(event.getRequestTag());
    }



}
